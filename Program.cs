﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Permissions;


// TODO: change `var` into explicit type
namespace UnsafeFeatures
{
    class ArglistCall
    {
        static void Foo(__arglist)
        {
            ArgIterator iter = new ArgIterator(__arglist);
            for (int n = iter.GetRemainingCount(); n > 0; n--)
                Console.WriteLine(TypedReference.ToObject(iter.GetNextArg()));
        }

        public static void MainFunc(string[] args)
        {
            Foo(__arglist(1, 2, 3));
        }
    }

    class ArglistVSParams
    {
        static void Bar(params int[] vls)
        {
            Console.WriteLine(vls[0]);
        }

        static void Foo(__arglist)
        {
            ArgIterator iter = new ArgIterator(__arglist);
            Console.WriteLine(TypedReference.ToObject(iter.GetNextArg()));
        }

        public static void CallFoo()
        {
            Foo(__arglist(1, 2, 3));
        }

        public static void CallBar()
        {
            Bar(1, 2, 3);
        }

        static unsafe void GetAllIntArts(__arglist)
        {
            ArgIterator iter = new ArgIterator(__arglist);
            var p = (int***) &iter;
            var size = ((long) **p - (long) *p) / 8 - 2;
            Console.WriteLine(size);
            for (int i = 1; i < size + 1; i++)
            {
                Console.WriteLine("{0}: {1}", i, (int)*(p[3] + i));
            }
        }

        public static void CallGetAllIntArts()
        {
            GetAllIntArts(__arglist(42));
            GetAllIntArts(__arglist(42, 37));
            GetAllIntArts(__arglist(42, 37, 15));
        }

        public static void MainFunc(string[] args)
        {
            CallFoo();
            CallBar();
            Console.WriteLine();
            CallGetAllIntArts();
        }
    }

    public class CallPrintf // TODO: report to Mono
    {
        [DllImport("libc.so.6")]
        private static extern int printf(string format, __arglist);

        public static void MainFunc()
        {
            printf("Hello %s!\n", __arglist("Bart"));
        }
    }

    public class SimpleRefsExample
    {
        public static int GetTypeToken(TypedReference tr)
        {
            var rh = TypedReference.TargetTypeToken(tr);
            return (int)rh.Value;
        }

        public static void MainFunc()
        {
            int x = 3;
            TypedReference xRef = __makeref(x);
            Console.WriteLine(__reftype(xRef)); // Prints "System.Int32"
            Console.WriteLine(__refvalue(xRef, int)); // Prints "3"

            __refvalue(xRef, int) = 10;
//            __refvalue(xRef, double) = 0.3; // cast not valid
//            __refvalue(xRef, uint) = 10u; // cast not valid
            Console.WriteLine(__refvalue(xRef, int)); // Prints "10"
        }
    }

    public class YourOwnSizeOf
    {
        public static unsafe int SizeOf<T>() where T : struct
        {
            T[] tArray = new T[2];

            var tRef0 = __makeref(tArray[0]);
            var tRef1 = __makeref(tArray[1]);

            IntPtr ptrToT0 = *((IntPtr*)&tRef0 + 1);
            IntPtr ptrToT1 = *((IntPtr*)&tRef1 + 1);

            return (int)(((byte*)ptrToT1) - ((byte*)ptrToT0));
        }

        public struct struct1
        {
            private int x;
            private bool y;
            private int z;
        }

        public static unsafe void MainFunc(string[] args)
        {
            Console.WriteLine(SizeOf<struct1>());                // 12
            Console.WriteLine(sizeof(struct1));                  // 12
            Console.WriteLine(Marshal.SizeOf(typeof(struct1)));  // 12
        }
    }

    public class FunnyExceptionWithTypedReference // TODO: report to Mono
    {
        public static unsafe void MainFunc(string[] args)
        {
            var s = new YourOwnSizeOf.struct1();
            var p = &s; // nice

            var sRef = __makeref(s);
            var pp = &sRef;
            // System.BadImageFormatException: Expected value type but got type kind 22
            // https://github.com/mono/mono/blob/e47b1865c25e0d06a67778d5adbe1d51a33e5396/mono/metadata/metadata.c#L3552
        }
    }

    static class RefVSBoxingBench
    {
        static void Set1<T>(T[] a, int i, T v)
        { __refvalue(__makeref(a[i]), T) = v; }

        static void Set2<T>(T[] a, int i, int v)
        { a[i] = (T)(object)v; }

        public static void MainFunc(string[] args)
        {
            var root = new List<object>();
            var rand = new Random();
            for (int i = 0; i < 1024; i++)
            { root.Add(new byte[rand.Next(1024 * 64)]); }
            //The above code is to put just a bit of pressure on the GC

            var arr = new int[5];
            int start;
            const int COUNT = 40000000;


            start = Environment.TickCount;
            for (int i = 0; i < COUNT; i++)
            { Set2(arr, 0, i); }
            Console.WriteLine("Using boxing/unboxing: {0} ticks",
                Environment.TickCount - start);

            start = Environment.TickCount;
            for (int i = 0; i < COUNT; i++)
            { Set1(arr, 0, i); }
            Console.WriteLine("Using TypedReference:  {0} ticks",
                Environment.TickCount - start);

            // Using boxing/unboxing: 379 ticks
            // Using TypedReference:  154 ticks
        }
    }

    static class PrintInternPool
    {
        public static unsafe void PrintRange(char* c)
        {
            for (int i = -1000; i < 1000; i++)
            {
                Console.Write(c[i]);
            }
            Console.Write("\n");

        }

        public static unsafe void MainFunc(string[] args)
        {
            var s1 = "hello";
            var s2 = "world";

            fixed (char* c1 = s1)
            {
                PrintRange(c1);
            }
        }
    }

    public class Buffers
    {
        public unsafe struct FixedSizedBuffer
        {
            public fixed char buf[20];
            public fixed bool bufs[29];
        }

        public static unsafe void MainFunc(string[] args)
        {
            int[] a = {0, 1, 2};
            var f = new FixedSizedBuffer();

            Console.WriteLine("{0,20}", sizeof(FixedSizedBuffer));
            Console.WriteLine("{0,20}", Marshal.SizeOf(typeof(FixedSizedBuffer)));
            Console.WriteLine("{0,20}", YourOwnSizeOf.SizeOf<FixedSizedBuffer>());

//            int x = a[-1]; // System.IndexOutOfRangeException
            int y = f.buf[-1]; // nice
        }
    }

    public class StackAllocs
    {
        public static unsafe void AccessDuStackAllocs()
        {
            var smstck = stackalloc int[3]; // filled to 4
            var nestck = stackalloc int[5]; // filled to 8
            nestck[1] = 58;
            smstck[1] = 17;
            smstck[2] = 42;

            for (int i = -15; i < 15; i++)
            {
                Console.WriteLine("{0,30} {1,12} {2,12}", i, nestck[i], smstck[i-8]);
            }
        }

        public static unsafe void MainFunc(string[] args)
        {
            var emptystck = stackalloc int[0];
//            Console.WriteLine("{0}", (int)emptystck); // implementation defined
//            *stck = 42;

            AccessDuStackAllocs();
        }
    }

    public class ArrayNegLowerBound // TODO: broken
    {
        public static unsafe void MainFunc(string[] args)
        {
            int[] l = {30};
            int[] s = {12};
            var a = Array.CreateInstance(typeof(int), l, s);

            int[] b = a as int[];

            fixed (int* p = b)
            {
                for (int i = -15; i < 15; i++)
                {
                    Console.WriteLine("{0} {1}", i, p[i]);
                }
            }

//            b[0] = 42;
//            Console.WriteLine(b[-10]);
        }
    }

    public class OffsetBuffers
    {
        [StructLayout(LayoutKind.Explicit)]
        public struct struct1
        {
            [FieldOffset(0)]
            public byte a;  // 1 byte
            [FieldOffset(1)]
            public int b;   // 4 bytes
            [FieldOffset(5)]
            public short c; // 2 bytes
            [FieldOffset(7)]
            public byte buffer;
            [FieldOffset(18)]
            public byte d;  // 1 byte
        }

        public static unsafe void MainFunc(string[] args)
        {
            var s = new struct1();
            s.c = 259;
            s.d = 37;
            s.buffer = 14;
            byte* p = (byte*) &s;
            for (byte i = 0; i < 20; i++)
            {
                Console.WriteLine("{0,20} {1}", i, p[i]);
            }
        }
    }

    static class Program
    {
        public static unsafe void Main(string[] args)
        {
            YourOwnSizeOf.MainFunc(args);
        }
    }
}
